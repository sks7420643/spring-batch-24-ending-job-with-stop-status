package com.emrah.configuration;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BatchConfiguration {

	private final JobBuilderFactory jobBuilderFactory;
	private final StepBuilderFactory stepBuilderFactory;

	@Bean
	public Tasklet passTasklet() {
		return (contribution, chunkContext) -> {
			throw new RuntimeException("Causing a failure");
			//return RepeatStatus.FINISHED;
		};
	}
	

	@Bean
	public Tasklet successTasklet() {
		return (contribution, chunkContext) -> {
			System.out.println("Success");
			return RepeatStatus.FINISHED;
		};
	}
	

	@Bean
	public Step firstStep() {
		return stepBuilderFactory.get("firstStep")
				.tasklet(passTasklet())
				.build();
	}
	

	@Bean
	public Step successStep() {
		return stepBuilderFactory.get("successStep")
				.tasklet(successTasklet())
				.build();
	}
	
	@Bean
	public Job job() {
		return jobBuilderFactory.get("job24")
				.start(firstStep()).on("FAILED").stopAndRestart(successStep())
				.from(firstStep()).on("*").to(successStep())
				.end()
				.build();
	}
	
}
